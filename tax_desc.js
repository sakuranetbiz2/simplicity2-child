function tax_desc_trigger_save() {
	var tax_desc = (typeof tinyMCE != "undefined") && tinyMCE.activeEditor && !tinyMCE.activeEditor.isHidden();
	if (tax_desc) {
		ed = tinyMCE.activeEditor;
		if ( 'mce_fullscreen' == ed.id || 'wp_mce_fullscreen' == ed.id ) {
			tinyMCE.get(0).setContent(ed.getContent({format : 'raw'}), {format : 'raw'});
		}
		tinyMCE.triggerSave();
	}
}

jQuery(document).ready(function($) {
	$('.form-field').has('#tag-description').remove();
	$('.form-field').has('#category-description').remove();
	$('.form-field').has('#description').remove();

	// Make sure you're saving the latest content
	$('input#submit').click(function(e) {	
		tax_desc_trigger_save();
	});
});