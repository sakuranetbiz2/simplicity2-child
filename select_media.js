jQuery('document').ready(function(){
    jQuery('.media-upload').each(function(){//aタグの画像編集ボタンのそれぞれについて
        var rel = jQuery(this).attr("rel");//rel属性（すなわちid,nameと同じ）
	var imgfile = "";
        jQuery(this).click(function(){//クリックイベントをバインド
            window.send_to_editor = function(html) {//send_to_editor(html)定義
                imgurl = jQuery(html).attr('src');//引数html内のimgタグのsrcを得る
                jQuery('#pickup_image').val(imgurl);//ここでinputの内容を書き込んでいる
                tb_remove();//自分自身を消す
            }
            formfield = jQuery('#'+rel).attr('name');//inputフィールド
            tb_show(null, 'media-upload.php?post_id=0&type=image&TB_iframe=true');//iframeを開く
            return false;
        });
    });
})