<?php //子テーマ用関数

//親skins の取得有無の設定
function include_parent_skins(){
  return true; //親skinsを含める場合はtrue、含めない場合はfalse
}

//子テーマ用のビジュアルエディタースタイルを適用
add_editor_style();

//以下にSimplicity子テーマ用の関数を書く

/*　カテゴリーページの title、meta description、meta keyword に関する設定
　　All In One SEO を上書きしていますので、All In One SEOを入れないと機能しません　*/
function category_title( $title ) {
    if ( is_category() && function_exists( 'get_field' ) ) {
        $post_category = get_the_category();
        $category_id = $post_category[0]->cat_ID;

        $title = get_field( 'category_title', 'category_' . $category_id );
    }

    return $title;
}
add_filter( 'aioseop_title', 'category_title' );

function category_meta_description( $description ) {
    if ( is_category() && function_exists( 'get_field' ) ) {
        $post_category = get_the_category();
        $category_id = $post_category[0]->cat_ID;

        $description = get_field( 'category_metadisc', 'category_' . $category_id );
    }

    return $description;
}
add_filter( 'aioseop_description', 'category_meta_description' );

function category_meta_keywords( $keywords ) {
    if ( is_category() && function_exists( 'get_field' ) ) {
        $post_category = get_the_category();
        $category_id = $post_category[0]->cat_ID;

        $keywords = get_field( 'category_metakey', 'category_' . $category_id );
    }

    return $keywords;
}
add_filter( 'aioseop_keywords', 'category_meta_keywords' );


//
register_sidebars(1,
  array(
  'name'=>'トップページの記事下',
  'id' => 'widget-home-bottom',
  'description' => 'トップページの記事下に表示されるウイジェット。設定しないと表示されません。',
  'before_widget' => '<aside id="%1$s" class="widget-home-bottom %2$s">',
  'after_widget' => '</aside>',
  'before_title' => '<div class="widget-home-bottom-title main-widget-label">',
  'after_title' => '</div>',
));

//検索ウィジェットのカスタマイズ
function new_search_form($form) {
$form = '<form method="get" id="searchform" action="' . get_option('home') . '/" >
<table>
<tbody>
<tr>
<td class="input_search_td"><input type="text" value="' . attribute_escape(apply_filters('the_search_query', get_search_query())) . '" name="s" id="s" /></td>
<td class="submit_search_td"><input type="image" id="searchsubmit" src="' . get_stylesheet_directory_uri() . '/images/roope.png" width="15" height="16" /></td>
</tr>
</tbody>
</table>
</form>';
return $form;
}
add_filter('get_search_form', 'new_search_form' );


class My_WP_Widget_Recent_Posts extends WP_Widget_Recent_Posts {
    public function widget($args, $instance) {
        $cache = array();
        if ( ! $this->is_preview() ) {
            $cache = wp_cache_get( 'widget_recent_posts', 'widget' );
        }
 
        if ( ! is_array( $cache ) ) {
            $cache = array();
        }
 
        if ( ! isset( $args['widget_id'] ) ) {
            $args['widget_id'] = $this->id;
        }
 
        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return;
        }
 
        ob_start();
 
        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts' );
 
        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
 
        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
        if ( ! $number )
            $number = 5;
        $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;
 
        /**
         * Filter the arguments for the Recent Posts widget.
         *
         * @since 3.4.0
         *
         * @see WP_Query::get_posts()
         *
         * @param array $args An array of arguments used to retrieve the recent posts.
         */
        $r = new WP_Query( apply_filters( 'widget_posts_args', array(
            'posts_per_page'      => $number,
            'no_found_rows'       => true,
            'post_status'         => 'publish',
            'ignore_sticky_posts' => true
        ) ) );
 
        if ($r->have_posts()) :
?>
        <?php echo $args['before_widget']; ?>
        <?php if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        } ?>
        <ul class="nav nav-pills nav-stacked">
        <?php while ( $r->have_posts() ) : $r->the_post(); global $post; setup_postdata( $post );  ?>
    <article class="related-entry cf">
      <div class="related-entry-thumb">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <?php if ( has_post_thumbnail() ): // サムネイルを持っているとき ?>
        <?php echo get_the_post_thumbnail($post->ID, 'thumb100', array('class' => 'related-entry-thumb-image', 'alt' => get_the_title()) ); //サムネイルを呼び出す?>
        <?php else: // サムネイルを持っていないとき ?>
        <img src="<?php echo get_template_directory_uri(); ?>/images/no-image.png" alt="NO IMAGE" class="no-image related-entry-no-image" />
        <?php endif; ?>
        </a>
      </div><!-- /.related-entry-thumb -->
      <div class="related-entry-content">
        <header>
          <h3 class="related-entry-title">
            <a href="<?php the_permalink(); ?>" class="related-entry-title-link" title="<?php the_title(); ?>">
            <?php the_title(); //記事のタイトル?>
            </a></h3>
        </header>
<p class="related-entry-snippet">
       <?php echo get_the_custom_excerpt( $post->post_content, get_excerpt_length() ) . ''; //カスタマイズで指定した文字の長さだけ本文抜粋?></p>
        <?php if ( get_theme_text_read_entry() ): //「記事を読む」のようなテキストが設定されている時 ?>
        <footer>
          <p class="related-entry-read"><a href="<?php the_permalink(); ?>"><?php echo get_theme_text_read_entry(); //デフォルト：記事を読む ?></a></p>
        </footer>
        <?php endif; ?>

      </div><!-- /.related-entry-content -->
    </article><!-- /.elated-entry -->
        <?php endwhile; ?>
        </ul>
        <?php echo $args['after_widget']; ?>
<?php
        // Reset the global $the_post as this query will have stomped on it
        wp_reset_postdata();
 
        endif;
 
        if ( ! $this->is_preview() ) {
            $cache[ $args['widget_id'] ] = ob_get_flush();
            wp_cache_set( 'widget_recent_posts', $cache, 'widget' );
        } else {
            ob_end_flush();
        }
    }
}
function wp_my_widget_register() {
    register_widget('My_WP_Widget_Recent_Posts');
}
add_action('widgets_init', 'wp_my_widget_register');

//PC or スマホ・タブレット条件分岐
// PCでのみ表示するコンテンツ
function if_is_pc($atts, $content = null )
{
$content = do_shortcode( $content);
 if(!wp_is_mobile())
 {
 return $content;
 }
}
add_shortcode('pc', 'if_is_pc');
// スマホ・タブレットでのみ表示するコンテンツ
function if_is_nopc($atts, $content = null )
{
$content = do_shortcode( $content);
 if(wp_is_mobile())
 {
 return $content;
 }
}
add_shortcode('nopc', 'if_is_nopc');

//PICKUP管理ページ
add_action('admin_print_scripts', 'my_admin_scripts');
add_action('admin_print_styles', 'my_admin_styles');

function my_admin_styles() {
    wp_enqueue_style('thickbox');
}
function my_admin_scripts() {
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_script('jquery');
}
function my_admin_script(){
    // body 終了タグ直前で jQuery のファイルを読み込みたい場合
    wp_enqueue_script( 'my_admin_script', get_stylesheet_directory_uri().'/select_media.js', array('jquery'), '', true);
}
add_action( 'admin_enqueue_scripts', 'my_admin_script' );

add_action('admin_menu', 'pickup_menu');

function pickup_menu() {
  add_menu_page('PICKUP管理', 'PICKUP管理', 8 ,'pickup_menu', 'pickup_options_page');
  add_action( 'admin_init', 'register_pickup_settings' );
}

function register_pickup_settings() {
  register_setting( 'pickup-settings-group', 'pickup_url' );
  register_setting( 'pickup-settings-group', 'pickup_text' );
  register_setting( 'pickup-settings-group', 'pickup_image' );
  register_setting( 'pickup-settings-group', 'reco_url1' );
  register_setting( 'pickup-settings-group', 'reco_text1' );
  register_setting( 'pickup-settings-group', 'reco_url2' );
  register_setting( 'pickup-settings-group', 'reco_text2' );
  register_setting( 'pickup-settings-group', 'reco_url3' );
  register_setting( 'pickup-settings-group', 'reco_text3' );
  register_setting( 'pickup-settings-group', 'reco_url4' );
  register_setting( 'pickup-settings-group', 'reco_text4' );
  register_setting( 'pickup-settings-group', 'reco_url5' );
  register_setting( 'pickup-settings-group', 'reco_text5' );
}

function pickup_options_page() {
?>
<style>
a.media-upload{
    color: #555;
    border-color: #ccc;
    background: #f7f7f7;
    -webkit-box-shadow: 0 1px 0 #ccc;
    box-shadow: 0 1px 0 #ccc;
    vertical-align: top;
    display: inline-block;
    text-decoration: none;
    font-size: 13px;
    line-height: 26px;
    height: 28px;
    margin: 0;
    padding: 0 10px 1px;
    cursor: pointer;
    border-width: 1px;
    border-style: solid;
    -webkit-appearance: none;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    white-space: nowrap;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
</style>
  <div class="wrap">
    <h2>PICKUP記事管理</h2>
    <form method="post" action="options.php">
      <?php 
        settings_fields( 'pickup-settings-group' );
        do_settings_sections( 'pickup-settings-group' );
      ?>
<?php if( $pickup_url != "" || $pickup_text != "" || $pickup_image != "" || $reco_text1 != "" || $reco_url1 != "" || $reco_text2 != "" || $reco_url2 != "" || $reco_text3 != "" || $reco_url3 != "" || $reco_text4 != "" || $reco_url4 != "" || $reco_text5 != "" || $reco_url5 != ""  ){ ?>
<div id="pickup_area">
	<h2>PICKUP記事</h2>
	<div class="first">
<?php if($pickup_url != "" && $pickup_text != "" && $pickup_image != ""){ ?>
	<p><a href="<?php echo $pickup_url; ?>"><img src="<?php echo $pickup_image; ?>"></a></p>
	<p><a href="<?php echo $pickup_url; ?>"><?php echo $pickup_text; ?></a></p>
<?php } ?>
	</div>
	<div class="second">
<?php if($reco_text1 != "" && $reco_url1 != ""){ ?>
	<p><a href="<?php echo $reco_url1; ?>"><?php echo $reco_text1; ?></a></p>
<?php } ?>
<?php if($reco_text2 != "" && $reco_url2 != ""){ ?>
	<p><a href="<?php echo $reco_url2; ?>"><?php echo $reco_text2; ?></a></p>
<?php } ?>
<?php if($reco_text3 != "" && $reco_url3 != ""){ ?>
	<p><a href="<?php echo $reco_url3; ?>"><?php echo $reco_text3; ?></a></p>
<?php } ?>
<?php if($reco_text4 != "" && $reco_url4 != ""){ ?>
	<p><a href="<?php echo $reco_url4; ?>"><?php echo $reco_text4; ?></a></p>
<?php } ?>
<?php if($reco_text5 != "" && $reco_url5 != ""){ ?>
	<p><a href="<?php echo $reco_url5; ?>"><?php echo $reco_text5; ?></a></p>
<?php } ?>
	</div>
</div>
<?php } ?>
    <h2>おすすめ記事設定</h2>
      <table class="form-table">
        <tbody>
          <tr>
            <th scope="row">
              <label for="pickup_text">タイトル</label>
            </th>
              <td><input type="text" id="pickup_text" class="regular-text" name="pickup_text" value="<?php echo get_option('pickup_text'); ?>"></td>
          </tr>
          <tr>
            <th scope="row">
              <label for="pickup_url">URL</label>
            </th>
              <td><input type="text" id="pickup_url" class="regular-text" name="pickup_url" value="<?php echo get_option('pickup_url'); ?>"></td>
          </tr>
          <tr>
            <th scope="row">
              <label for="pickup_image">画像URL(320×120px推奨)</label>
            </th>
              <td><input type="text" id="pickup_image" name="pickup_image" class="regular-text" value="<?php echo get_option('pickup_image'); ?>"></a></td>
          </tr>
        </tbody>
      </table>

    <h2>ランキング記事設定</h2>
      <table class="form-table">
        <tbody>
          <tr>
            <th scope="row">
              <label for="reco_text1">１位タイトル</label>
            </th>
              <td><input type="text" id="reco_text1" class="regular-text" name="reco_text1" value="<?php echo get_option('reco_text1'); ?>"></td>
          </tr>
          <tr>
            <th scope="row">
              <label for="reco_url1">１位URL</label>
            </th>
              <td><input type="text" id="reco_url1" class="regular-text" name="reco_url1" value="<?php echo get_option('reco_url1'); ?>"></td>
          </tr>
          <tr>
            <th scope="row">
              <label for="reco_text2">２位タイトル</label>
            </th>
              <td><input type="text" id="reco_text2" class="regular-text" name="reco_text2" value="<?php echo get_option('reco_text2'); ?>"></td>
          </tr>
          <tr>
            <th scope="row">
              <label for="reco_url2">２位URL</label>
            </th>
              <td><input type="text" id="reco_url2" class="regular-text" name="reco_url2" value="<?php echo get_option('reco_url2'); ?>"></td>
          </tr>
          <tr>
            <th scope="row">
              <label for="reco_text3">３位タイトル</label>
            </th>
              <td><input type="text" id="reco_text3" class="regular-text" name="reco_text3" value="<?php echo get_option('reco_text3'); ?>"></td>
          </tr>
          <tr>
            <th scope="row">
              <label for="reco_url3">３位URL</label>
            </th>
              <td><input type="text" id="reco_url3" class="regular-text" name="reco_url3" value="<?php echo get_option('reco_url3'); ?>"></td>
          </tr>
          <tr>
            <th scope="row">
              <label for="reco_text4">４位タイトル</label>
            </th>
              <td><input type="text" id="reco_text4" class="regular-text" name="reco_text4" value="<?php echo get_option('reco_text4'); ?>"></td>
          </tr>
          <tr>
            <th scope="row">
              <label for="reco_url4">４位URL</label>
            </th>
              <td><input type="text" id="reco_url4" class="regular-text" name="reco_url4" value="<?php echo get_option('reco_url4'); ?>"></td>
          </tr>
          <tr>
            <th scope="row">
              <label for="reco_text5">５位タイトル</label>
            </th>
              <td><input type="text" id="reco_text5" class="regular-text" name="reco_text5" value="<?php echo get_option('reco_text5'); ?>"></td>
          </tr>
          <tr>
            <th scope="row">
              <label for="reco_url5">５位URL</label>
            </th>
              <td><input type="text" id="reco_url5" class="regular-text" name="reco_url5" value="<?php echo get_option('reco_url5'); ?>"></td>
          </tr>
        </tbody>
      </table>
      <?php submit_button(); ?>
    </form>
  </div>
<?php
}